/*----------------------------------------
 VALIDATIONS JQUERY FORM
 ----------------------------------------*/
$(function () {

  /* RULES */
  $('#form-order').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function (element) {
      $('#popup-message').gpPopup();
    }
  });

  $('#second-form-order').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function (element) {
      $('#popup-message').gpPopup();
    }
  });

  $('#form-feedback').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function (element) {
      $('#popup-message').gpPopup();
    }
  });

})
