/*----------------------------------------
 TRANSITION SCROLL
 ----------------------------------------*/
$('.scroll').on('click', function (e) {
  // e.preventDefault();
  var anchor = $(this)
  $('html, body').stop().animate({
    scrollTop: $(anchor.attr('href')).offset().top
  }, 1000)
})
/*----------------------------------------
  PHONE MASK
----------------------------------------*/
$('.js-mask-number').mask('0#')
$('.js-mask-phone').mask('+7 (000) 000-00-00')
$('.js-mask-time').mask('00 : 00')
$('.js-mask-date').mask('00.00.0000')

/*----------------------------------------
  JS CONTACTS
----------------------------------------*/

var buttonContacts = $('.js-contacts-button')

function activeContactList () {
  $(this).toggleClass('dropdown-contacts__button_active')
  $(this).siblings('.js-contacts-list').toggleClass('dropdown-contacts__list_active')
}

function disabledContactList () {
  buttonContacts.removeClass('dropdown-contacts__button_active')
  buttonContacts.siblings('.js-contacts-list').removeClass('dropdown-contacts__list_active')
}

var clickEvent = ((document.ontouchstart !== null) ? 'click' : 'touchstart')

$(document).on(clickEvent, function (e) {
  if (!$(e.target).closest('.js-contacts').length) {
    disabledContactList()
  }
  e.stopPropagation()
})

buttonContacts.click(activeContactList)

/*----------------------------------------
  SLICK INIT
----------------------------------------*/

var carousel = $('.js-advantages');

carousel.slick({
  infinite: false,
  rows: 1,
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: false,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        infinite: true,
        slidesToShow: 3,
        dots: true
      }
    },
    {
      breakpoint: 768,
      settings: {
        infinite: true,
        slidesToShow: 2,
        dots: true
      }
    },
    {
      breakpoint: 601,
      settings: {
        infinite: true,
        slidesToShow: 1,
        dots: true
      }
    }
  ]
});

/*----------------------------------------
  HERO INIT
----------------------------------------*/

$('.js-hero').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  fade: true,
  swipe: false,
  autoplay: true,
  autoplaySpeed: 5000,
  pauseOnHover: false,
  asNavFor: '.js-hero-thumbnails',
  responsive: [
    {
      breakpoint: 768,
      settings: {
        fade: false,
        autoplay: false,
        swipe: true,
        dots: true
      }
    },
  ]
});

$('.js-hero-thumbnails').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.js-hero',
  swipe: false,
  autoplay: true,
  autoplaySpeed: 5000,
  pauseOnHover: false,
  focusOnSelect: true
});

/*----------------------------------------
  SLICK WITH CUSTOM JS
 ----------------------------------------*/

// slider
var $clients = $('.js-clients');

settings_slider = {
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: true,
  arrows: false,
  mobileFirst: true,
  responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2
      }
    },
  ]
}

// slick on mobile
function slick_on_mobile(slider, settings){
  $(window).on('load resize', function() {
    if ($(window).width() > 767) {
      if (slider.hasClass('slick-initialized')) {
        slider.slick('unslick');
      }
      return
    }
    if (!slider.hasClass('slick-initialized')) {
      return slider.slick(settings);
    }
  });
};

slick_on_mobile( $clients, settings_slider);

